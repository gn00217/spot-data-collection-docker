FROM arm64v8/ubuntu:18.04
WORKDIR /home
RUN : \
    && apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        software-properties-common \
    && add-apt-repository -y ppa:deadsnakes \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        python3.8-venv \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && :
RUN apt-get update && apt-get install apt-file -y && apt-file update && apt-get install vim -y
RUN apt-get update && apt-get install ffmpeg libsm6 libxext6  -y
RUN python3.8 -m venv /venv
ENV PATH=/venv/bin:$PATH
RUN python3 -V
RUN python3 --version
RUN python3 -m pip install --upgrade bosdyn-client bosdyn-mission bosdyn-choreography-client
RUN python3 -m pip list --format=columns | grep bosdyn
COPY requirements.txt .
COPY setup_data_format.bash .
COPY data_collection.py .
RUN python3 -m pip install -r requirements.txt