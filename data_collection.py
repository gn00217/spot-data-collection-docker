import argparse
import sys
import logging
import time

import cv2
import numpy as np
from datetime import datetime

import bosdyn.client
import bosdyn.client.util
import bosdyn.client.math_helpers as math_helpers
from bosdyn.api import image_pb2
from bosdyn.client.image import ImageClient, build_image_request
from bosdyn.client.graph_nav import GraphNavClient

LOGGER = logging.getLogger()

# data capture setup for navigation using AutoWalk 
class DataCapture():
    def __init__(self, robot): 
        self.image_client = robot.ensure_client(
            ImageClient.default_service_name)
        self.graph_nav_client = robot.ensure_client(
            GraphNavClient.default_service_name)

    def start(self, options):
        opt = options
        # define cameras you want to capture from
        image_request = [
            build_image_request(source, pixel_format=format)
            for source, format in [
                ("back_depth",  "PIXEL_FORMAT_DEPTH_U16"),
                ("back_fisheye_image", "PIXEL_FORMAT_RGB_U8"),
                ("frontleft_depth", "PIXEL_FORMAT_DEPTH_U16"),
                ("frontleft_fisheye_image", 'PIXEL_FORMAT_RGB_U8'),
                ("frontright_depth", "PIXEL_FORMAT_DEPTH_U16" ),
                ("frontright_fisheye_image", "PIXEL_FORMAT_RGB_U8"),
                ("left_depth", "PIXEL_FORMAT_DEPTH_U16"),
                ("left_fisheye_image", "PIXEL_FORMAT_RGB_U8"),
                ("right_depth", "PIXEL_FORMAT_DEPTH_U16"),
                ("right_fisheye_image", "PIXEL_FORMAT_RGB_U8")]
        ]

        while True:
            image_responses = self.image_client.get_image(image_request)
            state = self.graph_nav_client.get_localization_state()
            print('saving data...')
            self.save_images(image_responses, opt.output_path)
            self.save_localisation(state, opt.output_path)

    def save_localisation(self, state, output_path):
        gn_origin_tform_body = state.localization.seed_tform_body
        time_s = state.localization.timestamp.seconds
        time_n = state.localization.timestamp.nanos
        image_time = time_s + time_n / pow(10, 9)
        pose = math_helpers.SE3Pose.from_obj(gn_origin_tform_body)
        pose = pose.to_matrix()
        with open(f"{output_path}/poses/{image_time}.json", "w") as f:
            print(pose, file=f)

    def save_images(self, images, output_path):
        # byte encodings are provided by BD
        for image in images:
            num_bytes = 1
            if image.shot.image.pixel_format == image_pb2.Image.PIXEL_FORMAT_DEPTH_U16:
                dtype = np.uint16
                extension = ".png"
            else:
                if image.shot.image.pixel_format == image_pb2.Image.PIXEL_FORMAT_RGB_U8:
                    num_bytes = 3
                elif image.shot.image.pixel_format == image_pb2.Image.PIXEL_FORMAT_RGBA_U8:
                    num_bytes = 4
                elif image.shot.image.pixel_format == image_pb2.Image.PIXEL_FORMAT_GREYSCALE_U8:
                    num_bytes = 1
                elif image.shot.image.pixel_format == image_pb2.Image.PIXEL_FORMAT_GREYSCALE_U16:
                    num_bytes = 2
                dtype = np.uint8
                extension = ".jpg"

            img = np.frombuffer(image.shot.image.data, dtype=dtype)
            if image.shot.image.format == image_pb2.Image.FORMAT_RAW:
                try:
                    # attempt to reshape array into a RGB rows X cols shape.
                    img = img.reshape(
                        (image.shot.image.rows, image.shot.image.cols, num_bytes))
                except ValueError:
                    # unable to reshape the image data, trying a regular decode.
                    img = cv2.imdecode(img, -1)
            elif image.shot.image.pixel_format == image_pb2.Image.PIXEL_FORMAT_DEPTH_U16:
                cv_depth = np.frombuffer(image.shot.image.data, dtype=np.uint16)
                cv_depth = cv_depth.reshape(image.shot.image.rows,
                                            image.shot.image.cols)
                img = cv_depth
            else:
                img = cv2.imdecode(img, -1)

            image_time = image.shot.acquisition_time.seconds + image.shot.acquisition_time.nanos / pow(10, 9)
            image_saved_path = image.source.name
            image_saved_path = image_saved_path.replace("/", '')
            cv2.imwrite(f'{output_path}/images/{image_saved_path}/' +
                       image_saved_path + '_' + str(image_time) + extension, img)


def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--output_path',
                        help='path you output data to')
    bosdyn.client.util.add_base_arguments(parser)
    options = parser.parse_args(argv)

    sdk = bosdyn.client.create_standard_sdk('data_capture')
    robot = sdk.create_robot(options.hostname)
    bosdyn.client.util.authenticate(robot)
    robot.sync_with_directory()
    robot.time_sync.wait_for_sync()

    img_capture = DataCapture(robot)
    # you'll manually have to kill this when you're ready for 
    # collection to stop
    img_capture.start(options)

if __name__ == "__main__":
    if not main(sys.argv[1:]):
        sys.exit(1)

