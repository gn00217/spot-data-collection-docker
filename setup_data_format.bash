#!/bin/bash
DEVICE_MOUNT=/data-collection
SCENE_NAME=$1
SEQUENCE_RUN=$2

cd $DEVICE_MOUNT
pwd
mkdir -p "${SCENE_NAME}"
cd "${SCENE_NAME}"
pwd
mkdir -p "sequence_${SEQUENCE_RUN}"
cd "sequence_${SEQUENCE_RUN}"
pwd
mkdir -p images poses
cd images
pwd
mkdir -p back_depth back_fisheye_image frontleft_depth frontleft_fisheye_image frontright_depth frontright_fisheye_image left_depth left_fisheye_image right_depth right_fisheye_image